package wateradmin.models.water_paas;

import java.util.List;

public class RebberSchemeSerializeModel {
    public RebberSchemeModel model;
    public RebberSchemeNodeDesignModel node_design;
    public List<RebberSchemeNodeModel> nodes;
    public List<RebberSchemeRuleModel> rules;
}
